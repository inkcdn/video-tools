#encoding=utf-8

import socket
import sys
import re
import hashlib
import hmac
from rtmp_util import *


CLIENT_KEY = 'Genuine Adobe Flash Player 001'
SERVER_KEY = 'Genuine Adobe Flash Media Server 001'


class RTMPSocket:

    def __init__(self, url = None, addr = None):
        self.url = url
        self.chunk_size = 128
        self.in_chunk_size = 128
        self.addr = addr
        self.domain = ''
        self.app = ''
        self.stream = ''
        self.tcurl = ''

        self.fd = socket.socket()

        if url is None:
            return

        domain, app, stream = re.match('rtmp://([^/]+)/([^/]+)/([^/]+)', url, re.I).groups()

        self.domain = domain
        self.app = app
        self.stream = stream    # stream + args
        self.tcurl = 'rtmp://' + domain + '/' + app

        if self.addr is None:
            host = self.domain
            port = 1935

            n = self.domain.find(':')
            if n > 0:
                host = self.domain[:n]
                port = int(self.domain[n+1:])

            self.addr = (host, port)


    def handshake(self):
        rtmp_ver = '\x03'
        tm = '\x00\x00\x00\x00'
        cli_ver = '\x01\x02\x03\x04'
        offs = '\x10\x10\x10\x10'
        str1 = 'a' * (16*4)
        str2 = 'a' * (764 - 4 - 32 - 16*4)
        str3 = 'a' * 764

        msg = tm + cli_ver + offs + str1 + str2 + str3
        digest = hmac.new(CLIENT_KEY, msg, digestmod=hashlib.sha256).digest()

        c0c1 = rtmp_ver + tm + cli_ver + offs + str1 + digest + str2 + str3

        self.fd.connect(self.addr)
        self.fd.sendall(c0c1)

        hs_data = readN(self.fd, 1 + 1536 + 1536)

        s1 = hs_data[1:1537]

        srv_digest = rtmp_find_digest(s1, 8)
        if srv_digest == None:
            srv_digest = rtmp_find_digest(s1, 772)

        msg = 'b' * 1504
        digest = hmac.new(srv_digest, msg, digestmod=hashlib.sha256).digest()
        c2 = msg + digest
        self.fd.sendall(c2)


    def send_connect(self):
        cmds = [
            AMFElement(AMF_STRING, 'app', self.app),
            AMFElement(AMF_STRING, 'type', 'nonprivate'),
            AMFElement(AMF_STRING, 'flashVer', 'FMLE/3.0 (compatible; Lavf54.6.100)'),
            AMFElement(AMF_STRING, 'tcUrl', self.tcurl),
        ]

        elts = [
            AMFElement(AMF_STRING, None, 'connect'),
            AMFElement(AMF_NUMBER, None, 1.0),
            AMFElement(AMF_OBJECT, None, cmds),
        ]

        ss = AMF_Write(elts)
        rmsg = RTMPMessage(CSID_AMF_INI, 0, MSG_AMF_CMD, 0, ss)
        bin = rmsg.pack()

        self.fd.sendall(bin)


    def read_message(self):
        bh = readN(self.fd, 1)

        fmt = ord(bh) >> 6
        csid = ord(bh) & 0x3f
        ts = mlen = tpid = msid = 0 
        msg = ''

        if fmt == 0:        # absolute timestamp
            d = readN(self.fd, 11)
            ts = struct.unpack('>I', '\x00' + d[:3])[0]
            mlen = struct.unpack('>I', '\x00' + d[3:6])[0]
            tpid = ord(d[6])
            msid = struct.unpack('>I', d[7:])[0]

        elif fmt == 1:      # relative timestamp
            d = readN(self.fd, 7)
            ts = struct.unpack('>I', '\x00' + d[:3])[0]
            mlen = struct.unpack('>I', '\x00' + d[3:6])[0]
            tpid = ord(d[6])

        else:
            print 'wtf is it'
            return None

        if ts >= 0xffffff:
            d = readN(self.fd, 4)
            ts = struct.unpack('>I', d)[0]

        # print fmt, csid, ts, tpid, msid, mlen

        r = mlen
        if mlen > self.in_chunk_size:
            r = self.in_chunk_size

        msg += readN(self.fd, r)

        left = mlen - r
        while left > 0:
            r = left
            if left > self.in_chunk_size:
                r = self.in_chunk_size

            tmph = readN(self.fd, 1)
            msg += readN(self.fd, r)
            left -= r

        rmsg = RTMPMessage(csid, ts, tpid, msid, msg)
        return rmsg


    def process_message(self, rmsg):
        if rmsg.tp == MSG_CHUNK_SIZE:
            self.in_chunk_size = struct.unpack('>I', rmsg.data)[0]

        else:
            pass


    def wait_connect_result(self):
        while True:
            rmsg = self.read_message()

            if rmsg.tp != MSG_AMF_CMD:
                self.process_message(rmsg)
                continue

            vec, pos = AMF_Read(rmsg.data, 4)
            if len(vec) < 4 or type(vec[3].data) != list:
                print "### `connect` command got illegal response ###"
                for item in vec:
                    print item.tostring()
                sys.exit()

            if vec[3].data[1].name != 'code' or vec[3].data[1].data != 'NetConnection.Connect.Success':
                print "### `connect` command got illegal response ###"
                print vec[3].data[1].tostring()
                sys.exit()

            break


    def send_create_stream(self):
        elts = [
            AMFElement(AMF_STRING, None, 'createStream'),
            AMFElement(AMF_NUMBER, None, 4.0),
            AMFElement(AMF_NULL, None, None),
        ]

        ss = AMF_Write(elts)
        rmsg = RTMPMessage(CSID_AMF_INI, 0, MSG_AMF_CMD, 0, ss)
        bin = rmsg.pack()

        self.fd.sendall(bin)


    def wait_create_stream_result(self):
        while True:
            rmsg = self.read_message()

            if rmsg.tp != MSG_AMF_CMD:
                self.process_message(rmsg)
                continue

            vec, pos = AMF_Read(rmsg.data, 4)
            for item in vec:
                # print item.tostring()
                pass

            break


    def send_publish(self):
        elts = [
            AMFElement(AMF_STRING, None, 'publish'),
            AMFElement(AMF_NUMBER, None, 5.0),
            AMFElement(AMF_NULL, None, None),
            AMFElement(AMF_STRING, None, self.stream),
            AMFElement(AMF_STRING, None, 'live'),
        ]

        ss = AMF_Write(elts)
        rmsg = RTMPMessage(CSID_AMF, 0, MSG_AMF_CMD, 1, ss)
        bin = rmsg.pack()

        self.fd.sendall(bin)


    def wait_publish_on_status(self):
        while True:
            rmsg = self.read_message()

            if rmsg.tp != MSG_AMF_CMD:
                self.process_message(rmsg)
                continue

            vec, pos = AMF_Read(rmsg.data, 4)

            if len(vec) < 4 or type(vec[3].data) != list:
                print "### `publish` command got illegal response ###"
                for item in vec:
                    print item.tostring()
                sys.exit()

            if vec[3].data[1].name != 'code' or vec[3].data[1].data != 'NetStream.Publish.Start':
                print "### `publish` command got illegal response ###"
                print vec[3].data[1].tostring()
                sys.exit()

            break


    def send_message(self, data):
        try:
            self.fd.sendall(data)

        except Exception, msg:
            print msg
            sys.exit()


    def close(self):
        self.fd.close()



def rtmp_find_digest(data, base):
    offs = ord(data[base])
    offs += ord(data[base+1])
    offs += ord(data[base+2])
    offs += ord(data[base+3])
    offs %= 728
    offs += base + 4

    msg = data[:offs] + data[offs+32:]
    srv_digest = data[offs: offs+32]
    digest = hmac.new(SERVER_KEY, msg, digestmod=hashlib.sha256).digest()
    if digest == srv_digest:
        return digest

    return None
