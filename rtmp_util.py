#encoding=utf-8

import sys
import struct


"""
For AMF
"""

AMF_NUMBER      = 0
AMF_BOOLEAN     = 1
AMF_STRING      = 2
AMF_OBJECT      = 3
AMF_NULL        = 5
AMF_ARRAY_NULL  = 6
AMF_MIXED_ARRAY = 8
AMF_END         = 9
AMF_ARRAY       = 10


class AMFElement:

    def __init__(self, t = None, n = None, d = None):
        self.tp = t
        self.name = n
        self.data = d

    def pack(self):
        res = chr(self.tp)

        if self.tp == AMF_NUMBER:
            res += struct.pack('>d', self.data)

        elif self.tp == AMF_BOOLEAN:
            res += chr(self.data)

        elif self.tp == AMF_STRING:
            res += struct.pack('>H', len(self.data)) + self.data

        elif self.tp == AMF_NULL or self.tp == AMF_ARRAY_NULL:
            pass

        elif self.tp == AMF_MIXED_ARRAY:
            res += '\x00\x00\x00\x00'

            for item in self.data:         # must be array
                res += struct.pack('>H', len(item.name))
                res += item.name
                res += item.pack()

            res += '\x00\x00'
            res += chr(AMF_END)

        elif self.tp == AMF_OBJECT:
            for item in self.data:         # must be array
                res += struct.pack('>H', len(item.name))
                res += item.name
                res += item.pack()

            res += '\x00\x00'
            res += chr(AMF_END)

        elif self.tp == AMF_ARRAY:
            res += struct.pack('>I', len(self.data))
            for item in self.data:         # must be array
                res += item.pack()

        else:
            print 'what fk is it!!!'

        return res

    def unpack(self, d):     # type and name is already set previously
        self.tp = ord(d[0])
        n = 1

        if self.tp == AMF_NUMBER:
            self.data = struct.unpack('>d', d[n:n+8])[0]
            n += 8

        elif self.tp == AMF_BOOLEAN:
            self.data = ord(d[n])
            n += 1

        elif self.tp == AMF_STRING:
            l = struct.unpack('>H', d[n:n+2])[0]
            n += 2
            self.data = d[n:n+l]
            n += l

        elif self.tp == AMF_NULL or self.tp == AMF_ARRAY_NULL:
            n += 0

        elif self.tp in [AMF_MIXED_ARRAY, AMF_OBJECT]:
            self.data = []

            if self.tp == AMF_MIXED_ARRAY:
                n += 4

            while n < len(d):
                l = struct.unpack('>H', d[n: n+2])[0]
                n += 2

                if l == 0:
                    break

                nm = d[n: n+l]
                n += l

                arr, t = AMF_Read(d[n:], 1)
                arr[0].name = nm
                self.data.append(arr[0])

                n += t

            n += 1

        elif self.tp == AMF_ARRAY:
            cnt = struct.unpack('>I', d[n: n+4])[0]
            n += 4

            arr, t = AMF_Read(d[n:], cnt)
            n += t

            self.data = arr

        else:
            print 'wtf is it'

        return n

    def tostring(self):
        if self.tp == AMF_NUMBER:
            return 'Number ' + str(self.data)

        elif self.tp == AMF_BOOLEAN:
            return 'Boolean ' + str(self.data)

        elif self.tp == AMF_STRING:
            return 'String ' + self.data

        elif self.tp in [AMF_OBJECT, AMF_MIXED_ARRAY]:
            res = 'Object\n'
            for item in self.data:
                res += 'Property ' + item.name + ' ' + item.tostring()
                res += '\n'

            return res

        elif self.tp == AMF_ARRAY:
            res = 'Array\n'
            for item in self.data:
                res += item.tostring()

            return res

        elif self.tp in [AMF_NULL, AMF_ARRAY_NULL]:
            res = 'NULL\n'

        else:
            print 'wtf is it'



def AMF_Read(data, total):
    vec = []
    num = 0
    pos = 0

    while pos < len(data):
        amfobj = AMFElement()
        n = amfobj.unpack(data[pos:])
        vec.append(amfobj)
        pos += n
        num += 1

        if num >= total:
            break

    return vec, pos


def AMF_Write(vec):
    res = ''
    for item in vec:
        res += item.pack()

    return res



"""
For RTMP Message
"""

MSG_CHUNK_SIZE      = 1
MSG_AMF_CMD         = 20

CSID_AMF_INI        = 3
CSID_AMF            = 5


class RTMPMessage:
    def __init__(self, csid=None, ts=None, tp=None, msid=None, msg=None):
        self.csid = csid
        self.ts = ts
        self.tp = tp
        self.msid = msid
        self.data = msg

    def pack(self, chunk_size = 128):
        ### use absolute timestamp here ###
        res = chr(self.csid)    # basic header use 1 byte
        if self.ts < 0xffffff:
            res += struct.pack('>I', self.ts)[1:]
        else:
            res += '\xff\xff\xff'

        res += struct.pack('>I', len(self.data))[1:]
        res += chr(self.tp)
        res += struct.pack('>I', self.msid)

        ## extended timestap ##
        if self.ts >= 0xffffff:
            res += struct.pack('>I', self.ts)

        res += self.data[0: chunk_size]
        pos = chunk_size

        th = 0xc0 | self.csid           # successive fragments header
        while pos < len(self.data):
            res += chr(th) + self.data[pos: pos+chunk_size]
            pos += chunk_size

        return res

    def show(self):
        print 'csid: ', self.csid
        print 'timestamp: ', self.ts
        print 'type: ', self.tp
        print 'msid: ', self.msid
        print 'len: ', len(self.data)


def readN(s, n):
    res = ''

    while n > 0:
        d = s.recv(n)
        n -= len(d)
        res += d

        if d is None or len(d) == 0:    # socket closed
            print 'socket error'
            sys.exit()

    return res



def print_hex(msg):
    for i in range(0, len(msg)):
        c = '%02x' % ord(msg[i])
        print c,
        if (i+1) % 16 == 0:
            print

