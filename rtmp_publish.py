#encoding=utf-8

import sys
import time
import struct
import getopt
import os

from rtmp_protocol import *


if __name__ == '__main__':

    # input
#    url = 'rtmp://up4.quanmin.tv/live/qhtest'
    url = 'rtmp://10.125.14.132:2935/live/qhtest?vhost=zz.com'
    fname = '/home/quehan/tmp/m.flv'
    fps = 24

    flush = 6

    rs = RTMPSocket(url)

    rs.handshake()
    rs.send_connect()

    rs.wait_connect_result()
    rs.send_create_stream()
    rs.wait_create_stream_result()

    rs.send_publish()
    rs.wait_publish_on_status()

    size = os.path.getsize(fname)

    n = 0
    gap = 1.0/(float(fps)/flush)

    begin = time.time()
    fh = open(fname, 'rb')

    fh.read(13)
    pos = 13

    while pos < size:
        hdr = fh.read(11)       # tag header
        tp = ord(hdr[0])
        dlen = struct.unpack('>I', '\x00' + hdr[1: 1+3])[0]
        ts = struct.unpack('>I', '\x00' + hdr[1+3: 1+3+3])[0]

        body = fh.read(dlen)    # tag body
        fh.read(4)              # previous tag size

        pos += 11 + dlen + 4

        rmsg = None

        if tp == 18:    # meta
            rmsg = RTMPMessage(CSID_AMF, 0, tp, 1, body)

        elif tp == 8:   # audio
            rmsg = RTMPMessage(4, ts, tp, 1, body)

        elif tp == 9:   # video
            rmsg = RTMPMessage(4, ts, tp, 1, body)

        if rmsg is not None:
            rs.send_message(rmsg.pack())

        if tp == 9:
            n += 1

            if n % fps == 0:
                now = time.time()
                diff = now - begin
                print float(n)/diff, '\t', float(diff)/60

                time.sleep(1.01)

    fh.close()
    rs.close()

