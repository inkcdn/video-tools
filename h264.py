#encoding=utf-8

import struct
import sys


BOUNDARY = '\x00\x00\x00\x01'


def print_nalu(nalu):
    hdr = ord(nalu[0])
    nal_type = hdr & 0x1f

    if nal_type == 7:
        print 'sps',

    elif nal_type == 8:
        print 'pps',

    elif nal_type == 5:
        print  'idr',

    else:
        print nal_type,

    print '\t', len(nalu)




def analyze_avc_header(data):
    configurationVersion = ord(data[0])
    avcProfileIndication = ord(data[1])
    profileCompatibility = ord(data[2])
    avcLevelIndication = ord(data[3])
    lengthSizeMinusOne = ord(data[4]) & 0x3

    numOfSPS = ord(data[5]) & 0x1f

    pos = 6
    for i in range(0, numOfSPS):
        sps_len = struct.unpack('>H', data[pos: pos+2])[0]
        sps_data = data[pos+2: pos+2+sps_len]

        print '\t',
        print_nalu(sps_data)

        pos += 2 + sps_len

    numOfPPS = ord(data[pos])
    pos += 1

    for i in range(0, numOfPPS):
        pps_len = struct.unpack('>H', data[pos: pos+2])[0]
        pps_data = data[pos+2: pos+2+pps_len]

        print '\t',
        print_nalu(pps_data)

        pos += 2 + sps_len


def analyze_avc_nalu(data):
    # len + data / 00 00 00 01 + data
    pos = 0
    data_len = len(data)
    vec = []

#    for i in range(0, 8):
#        print '%02x' % (ord(data[i])),
#
#    print

    if data[:4] == BOUNDARY:
        print '######## Boundary: 00 00 00 01 ##########'
        vec = data.split(BOUNDARY)

    else:
        while pos < data_len:
            dlen = struct.unpack('>I', data[pos: pos+4])[0]
            h264_data = data[pos+4: pos+4+dlen]
            pos += 4 + dlen

            if h264_data.find(BOUNDARY) == 0:
                print '######## Boundary Behind Length: 00 00 00 01 ##########'
                tmpv = h264_data.split(BOUNDARY)
                for item in tmpv:
                    if len(item) > 0:
                        vec.append(item)

            else:
                vec.append(h264_data)

    ### NAL Vec ###
    for item in vec:
        print '\t',
        print_nalu(item)


def analyze_videodata(data):
    frame_type = (ord(data[0]) & 0xf0) >> 4;
    codec_id = ord(data[0]) & 0x0f;

    if codec_id != 7:
        print 'Not h264'
        return

    avc_packet_type = ord(data[1])
    compositionTime = struct.unpack('>I', '\x00' + data[2: 5])[0]

    if frame_type == 1:
        print 'K',

    else:
        print 'C',

    if avc_packet_type == 0:
        print 'SequenceHeader',

    else:
        print 'NALU',

    print

    if avc_packet_type == 0:    # AVC sequence header
        analyze_avc_header(data[5:])

    else:                       # AVC NALU
        analyze_avc_nalu(data[5:])


if __name__ == '__main__':

    fh = open('/home/quehan/tmp/goal.flv', 'rb')
    ss = fh.read()
    fh.close()

    N = 6
    cnt = 0

    pos = 13
    sz = len(ss)

    while pos < sz:
        tp = ord(ss[pos])
        dlen = struct.unpack('>I', '\x00' + ss[pos+1: pos+1+3])[0]
        ts = struct.unpack('>I', '\x00' + ss[pos+1+3: pos+1+3+3])[0]
        data = ss[pos+11: pos+11+dlen]

        # print tp, ts, dlen

        if tp == 9:
            analyze_videodata(data)

            cnt += 1
            if cnt >= N:
                break

        pos += 11 + dlen + 4 

